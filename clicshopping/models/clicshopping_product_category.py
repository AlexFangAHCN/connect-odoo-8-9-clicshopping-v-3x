# -*- encoding: utf-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    This module copyright (C) 2014 e-imaginis
#    (http://www.clicshopping.org).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields
from openerp.tools.translate import _

class product_category(models.Model):
    _inherit = "product.category"
    _description = "Product Category ClicShopping"

# catégories
    clicshopping_categories_id = fields.Integer(string="Categorie Id", size=5, help="Id categories table of ClicShopping must be unique"),
    ClicShopping_categories_save_to_catalog = fields.Boolean("ClicShopping", help="Save the category inside ClicShopping"),
    clicshopping_categories_parent_id = fields.Integer(string="Categorie Parent Id", size=5, help="Parent Id categories table of ClicShopping must be unique"),
    clicshopping_categories_image = fields.Binary(string="Categorie Image"),
    clicshopping_categories_description = fields.Text(string="Description", translate=True),
    clicshopping_categories_head_title_tag = fields.Char(string="Categorie Seo title", translate=True, size=70, help="If it empty, default in ClicSshopping will be taken"),
    clicshopping_categories_head_desc_tag = fields.Char(string="Categorie Seo Description", translate=True, size=150, help="If it empty, default in ClicSshopping will be taken"),
    clicshopping_categories_head_keywords_tag = fields.Text(string="Categorie Seo Keywords", translate=True, help="If it empty, default in ClicSshopping will be taken"),

