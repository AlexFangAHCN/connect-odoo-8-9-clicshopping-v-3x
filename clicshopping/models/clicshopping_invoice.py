from openerp import fields, models
from openerp.tools.translate import _

class sale_order(models.Model):
    _inherit = "account.invoice"
    _description = "Invoice ClicShopping"


#order
    clicshopping_invoice_id = fields.Integer(string="Order Id", size=10, help="Id of the order"),
    clicshopping_invoice_reference = fields.Char(string="Order Reference", size=10, help="Reference of order save in ClicShopping"),
    clicshopping_invoice_date_purchased = fields.Char(string="Date Purchase", size=10, help="Date of order purchasing"),

    clicshopping_invoice_status = fields.Selection([('instance','Instance'),
                                                     ('processing','Processing'),
                                                     ('delivered','Delivered'),
                                                     ('cancelled','Cancelled')],'Status'),

    clicshopping_invoice_orders_archive = fields.Boolean(string="Order archive", size=20, help="Archive the order"),
#customer
    clicshopping_invoice_customer_id = fields.Integer(string="Customer Id", size=10, help="Id of the customer"),
    clicshopping_invoice_customers_group_id = fields.Integer(string="Customer Group", size=20, help="Customer Group"),
    clicshopping_invoice_customers_company = fields.Char(string="Customer Company name", size=20),
    clicshopping_invoice_customers_ape = fields.Char(string="Ape Number", size=20),
    clicshopping_invoice_customers_siret = fields.Char(string="Siret Number", size=20),
    clicshopping_invoice_customers_tva_intracom = fields.Char(string="Intracom number", size=20),
    clicshopping_invoice_customer_comments = fields.Text(string="Customer comments", help="Comments about this order"),
#localisation
    clicshopping_invoice_client_computer_ip = fields.Char(string="Customer computer IP", size=10, help="Ip of the customer"),
    clicshopping_invoice_provider_name_client = fields.Char(string="Customer provider Name", size=20, help="provider of the customer"),

#payment
    clicshopping_invoice_payment_method = fields.Char(string="Payment method", size=20,),
    clicshopping_invoice_cc_type = fields.Char(string="CC Type", size=20, help="Information credit card number"),
    clicshopping_invoice_cc_owner = fields.Char(string="CC Owner", size=20, help="Information credit card number"),
    clicshopping_invoice_cc_number = fields.Char(string="CC Number", size=20, help="Information credit card number"),
    clicshopping_invoice_cc_expires = fields.Char(string="CC Expires", size=20, help="Information credit card number"),
    
