# -*- encoding: utf-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    This module copyright (C) 2014 e-imaginis
#    (http://www.clicshopping.org).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models
from openerp.tools.translate import _


class clicshopping_customers_group(models.Model):
    _name = 'clicshopping.customers.group'
    _rec_name = 'clicshopping_customers_group_name'

    clicshopping_customers_group_id = fields.Integer(string="Group Id", size=5, help="Id customer group of ClicShopping must be unique"),
    clicshopping_customers_group_name = fields.Char(string="Group Name", size=30, required=True),
    clicshopping_customers_group_discount = fields.Float(string="Discount Group (%)", required=True, size=70, help='Group Name in percentage : (ex : 70 for 70%).'),
    clicshopping_customers_group_color_bar = fields.Char(string="Color Bar", size=20, help='(ex : eeeeee)'),
    clicshopping_customers_group_quantity_default = fields.Integer(string="Default Quantity", default='0', size=70, help='Default quantity for this group'),
    clicshopping_customers_group_group_order_taxe = fields.Boolean(string="Without taxes ?", size=70, help='You have a choice with taxes or without taxes'),
    clicshopping_customers_group_tax = fields.Boolean(string="taxe"),
    


class res_partner(models.Model):
    _inherit = 'res.partner'

    clicshopping_partner_customer_group_id = fields.Many2one('clicshopping.customers.group','Customers Group', help='Customers group.')

