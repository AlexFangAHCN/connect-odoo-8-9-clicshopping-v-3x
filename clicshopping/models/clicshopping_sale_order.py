# -*- encoding: utf-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    This module copyright (C) 2014 e-imaginis
#    (http://www.clicshopping.org).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields, models
from openerp.tools.translate import _

class sale_order(models.Model):
    _inherit = "sale.order"
    _description = "Order Sales ClicShopping"

#order
    clicshopping_order_id = fields.Integer(string="Order Id", size=10, help="Id of the order"),
    clicshopping_order_reference = fields.Char(string="Order Reference", size=10, help="Reference of order save in ClicShopping"),
    clicshopping_order_date_purchased = fields.Char(string="Date Purchase", size=10, help="Date of order purchasing"),

    clicshopping_order_status = fields.Selection([('instance','Instance'),
                                                   ('processing','Processing'),
                                                   ('delivered','Delivered'),
                                                   ('cancelled','Cancelled')],'Status'),

    clicshopping_order_orders_archive = fields.Boolean(string="Order archive", size=20, help="Archive the order"),
#customer
    clicshopping_order_customer_id = fields.Integer(string="Customer Id", size=10, help="Id of the customer"),
    clicshopping_order_customers_group_id = fields.Integer(string="Customer Group", size=20, help="Customer Group"),
    clicshopping_order_customers_company = fields.Char(string="Customer Company name", size=20),
    clicshopping_order_customers_ape = fields.Char(string="Ape Number", size=20),
    clicshopping_order_customers_siret = fields.Char(string="Siret Number", size=20),
    clicshopping_order_customers_tva_intracom = fields.Char(string="Intracom number", size=20),
    clicshopping_order_customer_comments = fields.Text(string="Customer comments", help="Comments about this order"),
#localisation
    clicshopping_order_client_computer_ip = fields.Char(string="Customer computer IP", size=10, help="Ip of customer"),
    clicshopping_order_provider_name_client = fields.Char(string="Customer provider Name", size=20, help="Provider of customer"),

#payment
    clicshopping_order_payment_method = fields.Char(string="Payment method", size=20,),
    clicshopping_order_cc_type = fields.Char(string="CC Type", size=20, help="Information credit card number"),
    clicshopping_order_cc_owner = fields.Char(string="CC Owner", size=20, help="Information credit card number"),
    clicshopping_order_cc_number = fields.Char(string="CC Number", size=20, help="Information credit card number"),
    clicshopping_order_cc_expires = fields.Char(string="CC Expires", size=20, help="Information credit card number"),
    
