# -*- encoding: utf-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    This module copyright (C) 2014 e-imaginis
#    (http://www.clicshopping.org).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import fields, models
from openerp.tools.translate import _


#class clicshopping_config_settings(models.Model):
class clicshopping_config_settings(models.TransientModel):
    _name = 'clicshopping.config.settings'
    _inherit = 'res.config.settings'

    clicshopping_config_database_name = fields.Char(string="Name of the Mysql database", size=50, required=True, help="Name of Mysql database of ClicShopping"),
    clicshopping_config_database_server = fields.Char(string="Mysql Server", size=50, required=True, help="Server of Mysql database of ClicShopping"),
    clicshopping_config_database_server_port = fields.Char(string="Port of the Mysql server", size=50, required=True, help="Port of the mysql of ClicShopping"),
    clicshopping_config_database_username = fields.Char(string="Username of the Mysql database", size=50, required=True, help="Username of Mysql database of ClicShopping"),
    clicshopping_config_database_password = fields.Char(string="Password of the Mysql database", size=50, required=True, help="Password of  Mysql database of ClicShopping"),
    clicshopping_config_image_directory = fields.Char(string="Image directory", size=150, required=True, help="Absolute Directory : (ex: /var/www/mydomain/boutique/sources/image/ "),

    clicshopping_config_authentification = fields.Char(string="ClicShopping Authentification key", size=50, required=True, help="ClicShopping Authentification key"),

